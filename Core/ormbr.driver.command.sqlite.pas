{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.driver.command.sqlite;

interface

uses
  SysUtils,
  Rtti,
  ormbr.driver.command,
  ormbr.driver.register,
  ormbr.factory.interfaces,
  GpSQLBuilder;

type
  /// <summary>
  /// Classe de conex�o concreta com dbExpress
  /// </summary>
  TDriverSQLite = class(TDriverCommand)
  public
    constructor Create; override;
    destructor Destroy; override;
    function BuildSQLSelect(ASQL: IGpSQLBuilder; APageSize: Integer): string; override;
    function BuildSQLSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string; override;
    function BuildSQLSequence(AClass: TClass): string; override;
  end;

implementation

{ TDriverSQLite }

constructor TDriverSQLite.Create;
begin
  inherited;
end;

destructor TDriverSQLite.Destroy;
begin

  inherited;
end;

function TDriverSQLite.BuildSQLSelect(ASQL: IGpSQLBuilder; APageSize: Integer): string;
begin
  inherited BuildSQLSelect(ASQL, APageSize);
  if APageSize > -1 then
     Result := ASQL.AsString + ' LIMIT %u OFFSET %u'
  else
     Result := ASQL.AsString;
end;

function TDriverSQLite.BuildSQLSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string;
var
  oCriteria: IGpSQLBuilder;
begin
  oCriteria := GetCriteriaSelect(AClass, AID);
  if APageSize > -1 then
     Result := oCriteria.AsString + ' LIMIT %u OFFSET %u'
  else
     Result := oCriteria.AsString;
end;

function TDriverSQLite.BuildSQLSequence(AClass: TClass): string;
begin
  inherited;

end;

initialization
  TDriverRegister.RegisterDriver(dnSQLite, TDriverSQLite.Create);

end.

