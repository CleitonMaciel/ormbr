{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.objects.manager;

interface

uses
  Classes,
  SysUtils,
  Types,
  Generics.Collections,
  Windows,
  DB,
  Rtti,
  GpSQLBuilder,
  ormbr.command.factory,
  ormbr.factory.interfaces,
  ormbr.command.interfaces,
  ormbr.mapping.rttiutils;

type
  TObjectManager<M: class, constructor> = class abstract
  private
    procedure DoNotifyList(Sender: TObject; const Item: M; Action: TCollectionNotification);
  protected
    FList: TObjectList<M>;
    FBuildSQLCommand: IBuildSQLCommand;
  public
    constructor Create(const AConnection: IDBConnection; const ADriverName: TDriverName); virtual;
    destructor Destroy; override;
    procedure InsertCommand(AObject: TObject); virtual;
    procedure UpdateCommand(AObject: TObject); overload; virtual;
    procedure UpdateCommand(AObject: TObject; AModifiedFields: TDictionary<string, TField>); overload; virtual;
    procedure DeleteCommand(AObject: TObject); virtual;
    function SelectCommandAll(APageSize: Integer): IDBResultSet; virtual;
    function SelectCommandID(AID: TValue; APageSize: Integer): IDBResultSet; virtual;
    function SelectCommand(ASQL: IGpSQLBuilder; APageSize: Integer): IDBResultSet; virtual;
    function GetNextPacketCommand: IDBResultSet; virtual;
    function Add: M; virtual;
    function GetSequence: IDBResultSet;
    property List: TObjectList<M> read FList;
  end;

implementation

{ TObjectManager<M> }

constructor TObjectManager<M>.Create(const AConnection: IDBConnection; const ADriverName: TDriverName);
begin
  // Instancia a lista de Objects do tipo
  FList := TObjectList<M>.Create(True);
  FBuildSQLCommand := TSQLCommandFactory.Create(AConnection, ADriverName);
  FList.OnNotify := DoNotifyList;
end;

destructor TObjectManager<M>.Destroy;
begin
  FList.Free;
  inherited;
end;

function TObjectManager<M>.Add: M;
begin
   Result := FList.Items[FList.Add(M.Create)];
end;

procedure TObjectManager<M>.DeleteCommand(AObject: TObject);
begin
   FBuildSQLCommand.BuildSQLDelete(AObject);
end;

function TObjectManager<M>.SelectCommandAll(APageSize: Integer): IDBResultSet;
begin
   Result := FBuildSQLCommand.BuildSQLSelectAll(M, APageSize);
end;

function TObjectManager<M>.SelectCommandID(AID: TValue; APageSize: Integer): IDBResultSet;
begin
  Result := FBuildSQLCommand.BuildSQLSelectID(M, AID, APageSize);
end;

function TObjectManager<M>.GetNextPacketCommand: IDBResultSet;
begin
   Result := FBuildSQLCommand.BuildSQLNextPacket;
end;

function TObjectManager<M>.GetSequence: IDBResultSet;
begin
//  Result := FBuildSQLCommand.BuilderSQLSequence(AConnection
end;

function TObjectManager<M>.SelectCommand(ASQL: IGpSQLBuilder; APageSize: Integer): IDBResultSet;
begin
  Result := FBuildSQLCommand.BuildSQLSelect(ASQL, APageSize);
end;

procedure TObjectManager<M>.UpdateCommand(AObject: TObject);
begin
   FBuildSQLCommand.BuildSQLUpdate(AObject);
end;

procedure TObjectManager<M>.UpdateCommand(AObject: TObject; AModifiedFields: TDictionary<string, TField>);
begin
   FBuildSQLCommand.BuildSQLUpdate(AObject, AModifiedFields);
end;

procedure TObjectManager<M>.InsertCommand(AObject: TObject);
begin
  FBuildSQLCommand.BuildSQLInsert(AObject);
end;

procedure TObjectManager<M>.DoNotifyList(Sender: TObject; const Item: M;
  Action: TCollectionNotification);
begin
   case Action of
     cnAdded:
     begin
//        OutputDebugString(PChar('cnAdded'));
     end;
     cnRemoved:
     begin
//        OutputDebugString(PChar('cnRemoved'));
     end;
     cnExtracted:
     begin
//        OutputDebugString(PChar('cnExtracted'));
     end;
   end;
end;

end.
