{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.driver.register;

interface

uses
  Generics.Collections,
  ormbr.driver.command,
  ormbr.factory.interfaces;

type
  TDriverRegister = class
  strict private
    class var FDriver: TObjectDictionary<TDriverName,TDriverCommand>;
  private
    class constructor Create;
    class destructor Destroy;
  public
    class procedure RegisterDriver(const ADriverName: TDriverName; const ADriverSQL: TDriverCommand);
    class function GetDriver(const ADriverName: TDriverName): TDriverCommand;
  end;

implementation

class constructor TDriverRegister.Create;
begin
  FDriver := TObjectDictionary<TDriverName,TDriverCommand>.Create([doOwnsValues]);
end;

class destructor TDriverRegister.Destroy;
begin
  FDriver.Free;
  inherited;
end;

class function TDriverRegister.GetDriver(const ADriverName: TDriverName): TDriverCommand;
begin
  Result := FDriver[ADriverName];
end;

class procedure TDriverRegister.RegisterDriver(const ADriverName: TDriverName; const ADriverSQL: TDriverCommand);
begin
  FDriver.AddOrSetValue(ADriverName, ADriverSQL);
end;

end.

