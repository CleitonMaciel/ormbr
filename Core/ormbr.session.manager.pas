{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.session.manager;

interface

uses
  Classes,
  Generics.Collections,
  SysUtils,
  Windows,
  DB,
  Rtti,
  /// orm
  ormbr.objects.manager,
  ormbr.bind.mapping,
  ormbr.factory.interfaces,
  ormbr.mapping.explorer,
  GpSQLBuilder;

type
  /// <summary>
  /// M - Sess�o Abstract
  /// </summary>
  TSessionAbstract<M: class, constructor; C: IDBConnection> = class abstract
  private
   /// <summary>
   /// Instancia a class que mapea todas as class do tipo Entity
   /// </summary>
    FExplorer: TMappingExplorer;
   /// <summary>
   /// Instancia a class do tipo generics recebida
   /// </summary>
    FManager: TObjectManager<M>;
   /// <summary>
   /// Captura a conex�o passada como par�metro
   /// </summary>
    FFetchingRecords: Boolean;
    /// <summary>
    /// Controle de pagina��o vindo do banco de dados
    /// </summary>
    FPageSize: Integer;
    procedure Update(AObject: TObject; AModifiedFields: TDictionary<string, TField>); overload;
  protected
  public
    constructor Create(AConnection: C; APageSize: Integer = -1); virtual;
    destructor Destroy; override;
    procedure Insert(AObject: TObject);
    procedure Update(AObject: TObject); overload;
    procedure Delete(AObject: TObject);
    function Open(ASQL: IGpSQLBuilder; APageSize: Integer = -1): IDBResultSet; overload;
    function Open(AID: TValue): IDBResultSet; overload;
    function GetNextPacket: IDBResultSet;
    property Manager: TObjectManager<M> read FManager;
    property Explorer: TMappingExplorer read FExplorer;
    property FetchingRecords: Boolean read FFetchingRecords write FFetchingRecords;
  end;

  /// <summary>
  /// M - Sess�o DataSet
  /// </summary>
  TSessionDataSet<M: class, constructor; C: IDBConnection> = class(TSessionAbstract<M, C>)
  private
  public
  end;

  /// <summary>
  /// M - Sess�o Objeto
  /// </summary>
  TSessionManager<M: class, constructor; C: IDBConnection> = class(TSessionAbstract<M, C>)
  private
  public
  end;

implementation

uses
  ormbr.command.inserter,
  ormbr.command.updater,
  ormbr.command.deleter,
  ormbr.command.selecter;

{ TSessionAbstract<M> }

constructor TSessionAbstract<M, C>.Create(AConnection: C; APageSize: Integer = -1);
begin
   FPageSize := APageSize;
   FManager := TObjectManager<M>.Create(AConnection, AConnection.GetDriverName);
   FExplorer := TMappingExplorer.GetInstance;
end;

destructor TSessionAbstract<M, C>.Destroy;
begin
  FManager.Free;
  FExplorer := nil;
  inherited;
end;

procedure TSessionAbstract<M, C>.Update(AObject: TObject; AModifiedFields: TDictionary<string, TField>);
begin
  FManager.UpdateCommand(AObject, AModifiedFields);
end;

procedure TSessionAbstract<M, C>.Update(AObject: TObject);
begin
  FManager.UpdateCommand(AObject);
end;

procedure TSessionAbstract<M, C>.Insert(AObject: TObject);
begin
  FManager.InsertCommand(AObject);
end;

procedure TSessionAbstract<M, C>.Delete(AObject: TObject);
begin
  FManager.DeleteCommand(AObject);
end;

function TSessionAbstract<M, C>.GetNextPacket: IDBResultSet;
begin
  if not FFetchingRecords then
  begin
    Result := FManager.GetNextPacketCommand;
    if Result.FetchingAll then
      FFetchingRecords := True
  end;
end;

function TSessionAbstract<M, C>.Open(AID: TValue): IDBResultSet;
begin
  FFetchingRecords := False;
  Result := FManager.SelectCommandID(AID, 1);
end;

function TSessionAbstract<M, C>.Open(ASQL: IGpSQLBuilder; APageSize: Integer): IDBResultSet;
begin
  FFetchingRecords := False;
  if ASQL.AsString = '' then
    Result := FManager.SelectCommandAll(APageSize)
  else
    Result := FManager.SelectCommand(ASQL, APageSize);
end;

end.


