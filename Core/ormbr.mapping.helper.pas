{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.mapping.helper;

interface

uses
  Generics.Collections,
  Rtti,
  SysUtils,
  Types,
  TypInfo,
  ormbr.mapping.rttiutils,
  ormbr.mapping.attributes,
  ormbr.types.nullable;

type
  TObjectHelper = class helper for TObject
  public
    function &GetType(out AType: TRttiType): Boolean;
    function GetTable: TArray<TCustomAttribute>;
    function GetPrimaryKey: TArray<TRttiProperty>;
    function GetColumn: TArray<TRttiProperty>; overload;
    function GetColumn(AProperty: TRttiProperty): TCustomAttribute; overload;
    function GetJoinColumn(AProperty: TRttiProperty): TCustomAttribute; overload;
    function GetRestrictions(AProperty: TRttiProperty): TCustomAttribute; overload;
    function GetDictionary: TArray<TRttiProperty>; overload;
    function GetDictionary(AProperty: TRttiProperty): TCustomAttribute; overload;
  end;

  TRttiTypeHelper = class helper for TRttiType
  public
    function GetAssociation: TArray<TCustomAttribute>;
  end;

  TRttiPropertyHelper = class helper for TRttiProperty
  public
    function  IsJoinColumn: Boolean;
    function  GetNullableValue(AInstance: Pointer): TValue;
    procedure SetNullableValue(AInstance: Pointer; ATypeInfo: PTypeInfo; AValue: Variant);
  end;

implementation

var
  Context: TRttiContext;

{ TObjectHelper }

function TObjectHelper.GetTable: TArray<TCustomAttribute>;
var
  oRttiType: TRttiType;
  oRttiAttr: TCustomAttribute;
  iLength: Integer;
begin
   iLength := -1;
   if &GetType(oRttiType) then
   begin
      for oRttiAttr in oRttiType.GetAttributes do
      begin
         if oRttiAttr is Table then // Table
         begin
           Inc(iLength);
           SetLength(Result, iLength+1);
           Result[iLength] := oRttiAttr;
         end;
      end;
   end;
end;

{ TRttiPropertyHelper }

function TObjectHelper.GetColumn: TArray<TRttiProperty>;
var
  oRttiType: TRttiType;
  oProperty: TRttiProperty;
  oRttiAttr: TCustomAttribute;
  iLength: Integer;
begin
   iLength := -1;
   if &GetType(oRttiType) then
   begin
      for oProperty in oRttiType.GetProperties do
      begin
         for oRttiAttr in oProperty.GetAttributes do
         begin
            if (oRttiAttr is Column) then // Column
            begin
              Inc(iLength);
              SetLength(Result, iLength+1);
              Result[iLength] := oProperty;
            end;
         end;
      end;
   end;
end;

function TObjectHelper.GetDictionary: TArray<TRttiProperty>;
var
  oRttiType: TRttiType;
  oProperty: TRttiProperty;
  oRttiAttr: TCustomAttribute;
  iLength: Integer;
begin
   iLength := -1;
   if &GetType(oRttiType) then
   begin
      for oProperty in oRttiType.GetProperties do
      begin
         for oRttiAttr in oProperty.GetAttributes do
         begin
            if oRttiAttr is Dictionary then // Dictionary
            begin
              Inc(iLength);
              SetLength(Result, iLength+1);
              Result[iLength] := oProperty;
            end;
         end;
      end;
   end;
end;

function TObjectHelper.GetDictionary(AProperty: TRttiProperty): TCustomAttribute;
var
  oRttiAttr: TCustomAttribute;
begin
   for oRttiAttr in AProperty.GetAttributes do
   begin
      if oRttiAttr is Dictionary then // Dictionary
         Exit(oRttiAttr);
   end;
   Exit(nil);
end;

function TObjectHelper.GetJoinColumn(AProperty: TRttiProperty): TCustomAttribute;
var
  oRttiAttr: TCustomAttribute;
begin
   for oRttiAttr in AProperty.GetAttributes do
   begin
      if oRttiAttr is JoinColumn then // JoinColumn
         Exit(oRttiAttr);
   end;
   Exit(nil);
end;

function TObjectHelper.GetColumn(AProperty: TRttiProperty): TCustomAttribute;
var
  oRttiAttr: TCustomAttribute;
begin
   for oRttiAttr in AProperty.GetAttributes do
   begin
      if oRttiAttr is Column then // Column
         Exit(oRttiAttr);
   end;
   Exit(nil);
end;

function TObjectHelper.GetPrimaryKey: TArray<TRttiProperty>;
var
  oRttiType: TRttiType;
  oProperty: TRttiProperty;
  oRttiAttr: TCustomAttribute;
  iLength: Integer;
begin
   iLength := -1;
   if &GetType(oRttiType) then
   begin
      for oProperty in oRttiType.GetProperties do
      begin
         for oRttiAttr in oProperty.GetAttributes do
         begin
            if oRttiAttr is PrimaryKey then // PrimaryKey
            begin
              Inc(iLength);
              SetLength(Result, iLength+1);
              Result[iLength] := oProperty;
            end;
         end;
      end;
   end;
end;

function TObjectHelper.GetRestrictions(AProperty: TRttiProperty): TCustomAttribute;
var
  oRttiAttr: TCustomAttribute;
begin
   for oRttiAttr in AProperty.GetAttributes do
   begin
      if oRttiAttr is Restrictions then // Restrictions
         Exit(oRttiAttr);
   end;
   Exit(nil);
end;

function TObjectHelper.&GetType(out AType: TRttiType): Boolean;
begin
  Result := False;
  if Assigned(Self) then
  begin
    AType  := Context.GetType(Self.ClassType);
    Result := Assigned(AType);
  end;
end;

{ TRttiPropertyHelper }

function TRttiPropertyHelper.GetNullableValue(AInstance: Pointer): TValue;
var
  oValue: TValue;
  oValueField: TRttiField;
  oHasValueField: TRttiField;
begin
  if TRttiSingleton.GetInstance.IsNullable(Self.PropertyType.Handle) then
  begin
     oValue := Self.GetValue(AInstance);
     oHasValueField := Self.PropertyType.GetField('FHasValue');
     if Assigned(oHasValueField) then
     begin
       oValueField := Self.PropertyType.GetField('FValue');
       if Assigned(oValueField) then
          Result := oValueField.GetValue(oValue.GetReferenceToRawData);
     end
  end
  else
     Result := Self.GetValue(AInstance);
end;

function TRttiPropertyHelper.IsJoinColumn: Boolean;
var
  oRttiAttr: TCustomAttribute;
begin
   for oRttiAttr in Self.GetAttributes do
   begin
      if oRttiAttr is JoinColumn then // JoinColumn
         Exit(True);
   end;
   Exit(False);
end;

procedure TRttiPropertyHelper.SetNullableValue(AInstance: Pointer; ATypeInfo: PTypeInfo; AValue: Variant);
begin
   if ATypeInfo = TypeInfo(Nullable<Integer>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Integer>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<String>) then
      Self.SetValue(AInstance, TValue.From(Nullable<String>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<TDateTime>) then
      Self.SetValue(AInstance, TValue.From(Nullable<TDateTime>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<Currency>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Currency>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<Double>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Double>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<Boolean>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Boolean>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<TDate>) then
      Self.SetValue(AInstance, TValue.From(Nullable<TDate>.Create(AValue)))
end;

{ TRttiTypeHelper }

function TRttiTypeHelper.GetAssociation: TArray<TCustomAttribute>;
var
  oProperty: TRttiProperty;
  oAttrib: TCustomAttribute;
  iLength: Integer;
begin
   iLength := -1;
   for oProperty in Self.GetProperties do
   begin
      for oAttrib in oProperty.GetAttributes do
      begin
         if oAttrib is Association then // Association
         begin
           Inc(iLength);
           SetLength(Result, iLength+1);
           Result[iLength] := oAttrib;
         end;
      end;
   end;
end;

end.
