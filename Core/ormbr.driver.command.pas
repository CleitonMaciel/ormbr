{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.driver.command;

interface

uses
  DB,
  Rtti,
  SysUtils,
  StrUtils,
  Variants,
  TypInfo,
  Generics.Collections,
  ormbr.mapping.classes,
  ormbr.criteria.interfaces,
  ormbr.mapping.explorer,
  ormbr.mapping.helper,
  ormbr.mapping.attributes,
  ormbr.types.mapping,
  GpSQLBuilder;

type
  /// <summary>
  /// Classe de conex�es abstract
  /// </summary>
  TDriverCommand = class abstract
  private
    FDateFormat: string;
    FTimeFormat: string;
    function GetPropertyValue(AObject: TObject; AProperty: TRttiProperty): Variant;
    function IsNull(AProperty: TRttiProperty; AObject: TObject): Boolean;
    procedure SetJoinColumn(AClass: TClass; ATable: TTableMapping; ACriteria: IGpSQLBuilder);
  protected
    function GetCriteriaSelect(AClass: TClass; AID: TValue): IGpSQLBuilder; virtual;
    function GetBuildSQLSelect(ACriteria: IGpSQLBuilder): string; virtual;
  public
    constructor Create; virtual;
    function BuildSQLSelect(ASQL: IGpSQLBuilder; APageSize: Integer): string; virtual;
    function BuildSQLSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string; virtual; abstract;
    function BuildSQLUpdate(AObject: TObject): string; overload; virtual;
    function BuildSQLUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>): string; overload; virtual;
    function BuildSQLInsert(AObject: TObject): string; virtual;
    function BuildSQLDelete(AObject: TObject): string; virtual;
    function BuildSQLSequence(AClass: TClass): string; virtual; abstract;
    property DateFormat: string read FDateFormat write FDateFormat;
    property TimeFormat: string read FTimeFormat write FTimeFormat;
  end;

implementation

uses
  ormbr.command.selecter,
  ormbr.mapping.rttiutils;

{ TDriverCommand }

constructor TDriverCommand.Create;
begin
  FDateFormat := 'yyyy-MM-dd';
  FTimeFormat := 'HH:MM:SS';
end;

function TDriverCommand.BuildSQLDelete(AObject: TObject): string;
var
  oTableAttr: TCustomAttribute;
  oProperty: TRttiProperty;
  oCriteria: IGpSQLBuilder;
begin
   oCriteria := ICriteria.SQL.Delete;
   for oTableAttr in AObject.GetTable  do
   begin
      oCriteria.From(Table(oTableAttr).Name);
      for oProperty in AObject.GetPrimaryKey do
          oCriteria.Where(oProperty.Name + '=' + GetPropertyValue(AObject, oProperty));
   end;
   Result := oCriteria.AsString;
end;

function TDriverCommand.BuildSQLInsert(AObject: TObject): string;
var
  oClassAttr: TCustomAttribute;
  oRestAttr: TCustomAttribute;
  oProperty: TRttiProperty;
  oCriteria: IGpSQLBuilder;
begin
  Result := '';
  oCriteria := ICriteria.SQL;
  for oClassAttr in AObject.GetTable do
  begin
     oCriteria.Insert.Into(Table(oClassAttr).Name);
     for oProperty in AObject.GetColumn do
     begin
        if IsNull(oProperty, AObject) then
           Continue;
        /// <summary>
        /// Restrictions [Required, Unique, Check, NoInsert, NoUpdate, NotNull, Hidden]
        /// </summary>
        oRestAttr := AObject.GetRestrictions(oProperty);
        if oRestAttr <> nil then
          if NoInsert in Restrictions(oRestAttr).Restrictions then
            Continue;
        /// <summary>
        /// (Column, Value)
        /// </summary>
        oCriteria.&Set(oProperty.Name, GetPropertyValue(AObject,oProperty));
     end;
  end;
  Result := oCriteria.AsString;
end;

function TDriverCommand.BuildSQLSelect(ASQL: IGpSQLBuilder; APageSize: Integer): string;
begin
  Result := ASQL.AsString;
end;

function TDriverCommand.BuildSQLUpdate(AObject: TObject): string;
begin
  BuildSQLUpdate(AObject, nil);
end;

function TDriverCommand.GetBuildSQLSelect(ACriteria: IGpSQLBuilder): string;
begin

end;

function TDriverCommand.GetCriteriaSelect(AClass: TClass; AID: TValue): IGpSQLBuilder;
var
  oTable: TTableMapping;
  oColumnList: TColumnMappingList;
  oColumn: TColumnMapping;
  oPrimaryKeyList: TPrimaryKeyMappingList;
  oPrimaryKey: TPrimaryKeyMapping;
  oCriteria: IGpSQLBuilder;
begin
  oCriteria := ICriteria.SQL.Select;
  /// Table
  oTable := TMappingExplorer.GetInstance.GetMappingTable(AClass);
  oCriteria.From(oTable.Name);
  /// Columns
  oColumnList := TMappingExplorer.GetInstance.GetMappingColumn(AClass);
  for oColumn in oColumnList do
    oCriteria.Column(oTable.Name + '.' + oColumn.Name);
  /// JoinColumn
  SetJoinColumn(AClass, oTable, oCriteria);
  /// PrimaryKey
  oPrimaryKeyList := TMappingExplorer.GetInstance.GetMappingPrimaryKey(AClass);
  if oPrimaryKeyList <> nil then
  begin
    for oPrimaryKey in oPrimaryKeyList do
    begin
      if AID.AsInteger > 0 then
        oCriteria.Where(oPrimaryKey.ColumnName + '=' + IntToStr(AID.AsInteger));
      oCriteria.OrderBy(oPrimaryKey.ColumnName);
    end;
  end;
  Result := oCriteria;
end;

function TDriverCommand.GetPropertyValue(AObject: TObject; AProperty: TRttiProperty): Variant;
begin
  { TODO -oISAQUE : Tratar todos os tipo aqui }
  case TRttiSingleton.GetInstance.GetFieldType(AProperty.PropertyType.Handle) of
     ftString,ftWideString:
     begin
        Result := QuotedStr(AProperty.GetNullableValue(AObject).AsString)
     end;
     ftVariant:
     begin
        Result := VarToStr(AProperty.GetNullableValue(AObject).AsVariant);
     end;
     ftInteger,ftWord,ftSmallint:
     begin
        Result := IntToStr(AProperty.GetNullableValue(AObject).AsInteger);
     end;
     ftFloat:
     begin
        Result := FloatToStr(AProperty.GetNullableValue(AObject).AsExtended);
        Result := ReplaceStr(Result, ',', '.');
     end;
     ftDateTime,ftDate:
     begin
        Result := QuotedStr(FormatDateTime(FDateFormat, AProperty.GetNullableValue(AObject).AsExtended))
     end;
     ftTime,ftTimeStamp:
     begin
        Result := QuotedStr(FormatDateTime(FTimeFormat, AProperty.GetNullableValue(AObject).AsExtended))
     end;
     ftCurrency:
     begin
        Result := CurrToStr(AProperty.GetNullableValue(AObject).AsCurrency);
        Result := ReplaceStr(Result, ',', '.');
     end;
  else
     Result := '';
  end;
end;

function TDriverCommand.IsNull(AProperty: TRttiProperty;
  AObject: TObject): Boolean;
begin
  Result := False;
  if AProperty.PropertyType.TypeKind in [tkUnknown,tkEnumeration,tkClass,tkArray,tkDynArray,tkMethod,
                                         tkPointer,tkSet,tkClassRef,tkProcedure,tkInterface] then
     Exit(True);

  if AProperty.PropertyType.TypeKind in [tkString, tkUString] then
     if AProperty.GetValue(AObject).ToString = 'null' then
        Exit(True);

  if AProperty.PropertyType.TypeKind in [tkFloat] then
     if AProperty.PropertyType.Handle = TypeInfo(TDateTime) then
        if AProperty.GetValue(AObject).AsExtended = 0 then
           Exit(True);

  if AProperty.PropertyType.TypeKind in [tkFloat] then
     if AProperty.PropertyType.Handle = TypeInfo(TTime) then
        if AProperty.GetValue(AObject).AsExtended = 0 then
           Exit(True);
end;

procedure TDriverCommand.SetJoinColumn(AClass: TClass; ATable: TTableMapping; ACriteria: IGpSQLBuilder);
var
  oJoinList: TJoinColumnMappingList;
  oJoin: TJoinColumnMapping;
  oJoinExist: TList<string>;
begin
  oJoinExist := TList<string>.Create;
  try
    /// JoinColumn
    oJoinList := TMappingExplorer.GetInstance.GetMappingJoinColumn(AClass);
    if oJoinList <> nil then
    begin
      for oJoin in oJoinList do
      begin
        ACriteria.Column(oJoin.ReferencedTableName + '.' + oJoin.ColumnName);
        if oJoinExist.IndexOf(oJoin.ReferencedTableName) = -1 then
        begin
          oJoinExist.Add(oJoin.ReferencedTableName);
          /// Join Inner, Left, Right, Full
          if oJoin.Join = InnerJoin then
            ACriteria.InnerJoin(oJoin.ReferencedTableName).&On([oJoin.ReferencedTableName + '.' + oJoin.ReferencedColumnName,'=',ATable.Name + '.' + oJoin.ReferencedColumnName])
          else
          if oJoin.Join = LeftJoin then
            ACriteria.LeftJoin(oJoin.ReferencedTableName).&On([oJoin.ReferencedTableName + '.' + oJoin.ReferencedColumnName,'=',ATable.Name + '.' + oJoin.ReferencedColumnName])
          else
          if oJoin.Join = RightJoin then
            ACriteria.RightJoin(oJoin.ReferencedTableName).&On([oJoin.ReferencedTableName + '.' + oJoin.ReferencedColumnName,'=',ATable.Name + '.' + oJoin.ReferencedColumnName])
          else
          if oJoin.Join = FullJoin then
            ACriteria.FullJoin(oJoin.ReferencedTableName).&On([oJoin.ReferencedTableName + '.' + oJoin.ReferencedColumnName,'=',ATable.Name + '.' + oJoin.ReferencedColumnName]);
        end;
      end;
    end;
  finally
    oJoinExist.Free;
  end;
end;

function TDriverCommand.BuildSQLUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>): string;
var
  oClassAttr: TCustomAttribute;
  oRestAttr: TCustomAttribute;
  oProperty: TRttiProperty;
  oCriteria: IGpSQLBuilder;
  oCriteriaWhere: IGpSQLBuilder;
begin
   oCriteria := ICriteria.SQL;
   oCriteriaWhere := ICriteria.SQL;
   for oClassAttr in AObject.GetTable do
   begin
      oCriteria.Update(Table(oClassAttr).Name);
      for oProperty in AObject.GetPrimaryKey do
      begin
         oCriteriaWhere.Where(oProperty.Name + '=' + GetPropertyValue(AObject, oProperty));
      end;
      for oProperty in AObject.GetColumn do
      begin
        /// <summary>
        /// Restrictions [Required, Unique, Check, NoInsert, NoUpdate, NotNull, Hidden]
        /// </summary>
        oRestAttr := AObject.GetRestrictions(oProperty);
        if oRestAttr <> nil then
          if NoUpdate in Restrictions(oRestAttr).Restrictions then
            Continue;

        if AModifiedFields <> nil then
        begin
          if AModifiedFields.ContainsKey(oProperty.Name) then
          begin
            { TODO -oISAQUE : No futuro tenho que tratar para comparar o valor
                              do TField com o Valor da Propriedade }
//            if AModifiedFields.Items[oProperty.Name].OldValue <> AModifiedFields.Items[oProperty.Name].NewValue then
               oCriteria.&Set(oProperty.Name, GetPropertyValue(AObject, oProperty));
          end;
        end
        else
           oCriteria.&Set(oProperty.Name, GetPropertyValue(AObject, oProperty));
      end;
   end;
   Result := oCriteria.AsString + ' ' + oCriteriaWhere.AsString
end;

end.
