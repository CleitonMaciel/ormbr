{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.dependency.injection.fdmemtable;

interface

uses
  Classes,
  SysUtils,
  DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS,
  FireDAC.Phys.Intf,
  FireDAC.DApt.Intf,
  FireDAC.Stan.Async,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  /// ormbr
  ormbr.dependency.interfaces,
  ormbr.factory.interfaces,
  ormbr.bind.base,
  ormbr.bind.fdmemtable;

type
  TContainerFDMemTable<T: class, constructor> = class(TInterfacedObject, IContainerDataSet<T>)
  private
    FConnection: IDBConnection;
    FDataSetBase: TDataSetBase<T>;
    function GetDatasetBase: TDataSetBase<T>;
  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet; APageSize: Integer; AMasterObject: TObject); overload;
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet); overload;
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet; APageSize: Integer); overload;
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet; AMasterObject: TObject); overload;
    destructor Destroy; override;
    property DataSet: TDataSetBase<T> read GetDatasetBase;
  end;

implementation

{ TContainer }

constructor TContainerFDMemTable<T>.Create(AConnection: IDBConnection; ADataSet: TDataSet; APageSize: Integer; AMasterObject: TObject);
begin
  FConnection := AConnection;
  if ADataSet is TFDMemTable then
    FDataSetBase := TFDMemTableAdapter<T>.Create(AConnection, ADataSet, APageSize, AMasterObject)
  else
    raise Exception.Create('Is not TFDMemTable type');
end;

constructor TContainerFDMemTable<T>.Create(AConnection: IDBConnection; ADataSet: TDataSet; APageSize: Integer);
begin
  Create(AConnection, ADataSet, APageSize, nil);
end;

constructor TContainerFDMemTable<T>.Create(AConnection: IDBConnection; ADataSet: TDataSet);
begin
  Create(AConnection, ADataSet, -1, nil);
end;

constructor TContainerFDMemTable<T>.Create(AConnection: IDBConnection; ADataSet: TDataSet; AMasterObject: TObject);
begin
  Create(AConnection, ADataSet, -1, AMasterObject);
end;

destructor TContainerFDMemTable<T>.Destroy;
begin
  FDataSetBase.Free;
  inherited;
end;

function TContainerFDMemTable<T>.GetDatasetBase: TDataSetBase<T>;
begin
  Result := FDataSetBase;
end;

end.
