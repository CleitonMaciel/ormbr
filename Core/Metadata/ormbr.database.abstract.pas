{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.database.abstract;

interface

uses
  ormbr.factory.interfaces,
  ormbr.metadata.db.factory,
  ormbr.metadata.classe.factory,
  ormbr.database.mapping;

type
  TDatabaseAbstract = class abstract
  protected
    FCatalogMaster: TmkCatalogMetadata;
    FCatalogTarget: TmkCatalogMetadata;
    FMetadataDB: TMetadataDBAbstract;
    FMetadataObject: TMetadataClasseAbstract;
    FConnection: IDBConnection;
  public
    constructor Create(AConnection: IDBConnection); overload; virtual;
    destructor Destroy; override;
    property Connection: IDBConnection read FConnection;
  end;

implementation

{ TAbstractDatabase }

constructor TDatabaseAbstract.Create(AConnection: IDBConnection);
begin
  FConnection := AConnection;
  FCatalogMaster := TmkCatalogMetadata.Create;
  FCatalogTarget := TmkCatalogMetadata.Create;
  FMetadataDB := TMetadataDBFactory.Create(AConnection, FCatalogMaster);
  FMetadataObject := TMetadataClasseFactory.Create(FCatalogTarget);
end;

destructor TDatabaseAbstract.Destroy;
begin
  FCatalogMaster.Free;
  FCatalogTarget.Free;
  FMetadataDB.Free;
  inherited;
end;

end.

