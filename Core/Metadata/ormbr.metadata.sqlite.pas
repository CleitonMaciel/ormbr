{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.metadata.sqlite;

interface

uses
  SysUtils,
  Variants,
  ormbr.metadata.register,
  ormbr.metadata.extract,
  ormbr.database.mapping,
  ormbr.factory.interfaces,
  ormbr.mapping.rttiutils;

type
  TCatalogMetadataSQLite = class(TCatalogMetadataAbstract)
  protected
    function Execute: IDBResultSet;
  public
    procedure GetCatalogs;
    procedure GetSchemas; override;
    procedure GetTables; override;
    procedure GetColumns(ATable: TmkTable); override;
    procedure GetPrimaryKey(ATable: TmkTable); override;
    procedure GetIndexeKeys(ATable: TmkTable); override;
    procedure GetForeignKeys(ATable: TmkTable); override;
    procedure GetTriggers(ATable: TmkTable); override;
    procedure GetSequences; override;
    procedure GetProcedures; override;
    procedure GetFunctions; override;
    procedure GetViews; override;
  end;

implementation

{ TSchemaExtractSQLite }

function TCatalogMetadataSQLite.Execute: IDBResultSet;
var
  oSQLQuery: IDBQuery;
begin
  inherited;
  oSQLQuery := FConnection.CreateQuery;
  try
    oSQLQuery.CommandText := FSQL;
    Exit(oSQLQuery.ExecuteQuery);
  except
    raise
  end;
end;

procedure TCatalogMetadataSQLite.GetCatalogs;
//var
//  oDBResultSet: IDBResultSet;
begin
  inherited;
//  FSQL := 'pragma database_list';
//  oDBResultSet := Execute;
//  while oDBResultSet.NotEof do
//  begin
//    FCatalogMetadata.Name := VarToStr(oDBResultSet.GetFieldValue('catalog_name'));
//  end;
    FCatalogMetadata.Name := 'main';
end;

procedure TCatalogMetadataSQLite.GetSchemas;
begin
  inherited;
  FCatalogMetadata.Schema := 'schema'
end;

procedure TCatalogMetadataSQLite.GetTables;
var
  oSQL: TStringBuilder;
  oDBResultSet: IDBResultSet;
  oTable: TmkTable;
begin
  inherited;
  oSQL := TStringBuilder.Create;
  try
    oSQL.Append(' select name ');
    oSQL.Append(' from sqlite_master ');
    oSQL.Append(' where type = ''table''');
    oSQL.Append(' order by name ');
    FSQL := oSQL.ToString;
    oDBResultSet := Execute;
    while oDBResultSet.NotEof do
    begin
      oTable := TmkTable.Create(FCatalogMetadata);
      with oTable do
      begin
        Name := VarToStr(oDBResultSet.GetFieldValue('name'));
        /// <summary>
        /// Extrair colunas
        /// </summary>
        GetColumns(oTable);
        /// <summary>
        /// Extrair Primary Key
        /// </summary>
        GetPrimaryKey(oTable);
        /// <summary>
        /// Extrair Foreign Keys
        /// </summary>
        GetForeignKeys(oTable);
        /// <summary>
        /// Extrair Indexes
        /// </summary>
        GetIndexeKeys(oTable);
      end;
      FCatalogMetadata.Tables.Add(oTable);
    end;
  finally
    oSQL.Free;
  end;
end;

procedure TCatalogMetadataSQLite.GetColumns(ATable: TmkTable);
var
  oDBResultSet: IDBResultSet;
  oColumn: TmkTableField;
begin
  inherited;
  FSQL := Format('pragma table_info("%s")', [ATable.Name]);
  oDBResultSet := Execute;
  while oDBResultSet.NotEof do
  begin
    oColumn := oColumn.Create(ATable);
    with oColumn do
    begin
      Name := VarToStr(oDBResultSet.GetFieldValue('name'));
      Position := VarAsType(oDBResultSet.GetFieldValue('cid'), varInteger);
//      DataType: Integer;
//      TypeName: string;
//      Length: Integer;
//      Precision: Integer;
//      Scale: Integer;
      NotNull := oDBResultSet.GetFieldValue('notnull') = 1;
//      AutoIncrement: boolean;
      DefaultValue := VarToStr(oDBResultSet.GetFieldValue('dflt_value'));
    end;
    ATable.Fields.Add(oColumn);
  end;
end;

procedure TCatalogMetadataSQLite.GetPrimaryKey(ATable: TmkTable);
var
  oDBResultSet: IDBResultSet;
  oColumn: TmkTableField;
begin
  inherited;
  FSQL := Format('pragma table_info("%s")', [ATable.Name]);
  oDBResultSet := Execute;
  while oDBResultSet.NotEof do
  begin
    if VarAsType(oDBResultSet.GetFieldValue('pk'), varInteger) = 0 then
       Continue;
    oColumn := oColumn.Create(ATable);
    with oColumn do
    begin
      Name := VarToStr(oDBResultSet.GetFieldValue('name'));
      NotNull := oDBResultSet.GetFieldValue('notnull') = 1;
//      AutoIncrement: boolean;
    end;
    ATable.PrimaryKey.PrimaryKeyFields.Add(oColumn);
  end;
end;

procedure TCatalogMetadataSQLite.GetForeignKeys(ATable: TmkTable);
var
  oDBResultSet: IDBResultSet;
  oForeignKey: TmkForeignKey;
  oFromField: TmkTableField;
  oToField: TmkTableField;
  iID: Integer;
begin
  inherited;
  FSQL := Format('pragma foreign_key_list("%s")', [ATable.Name]);
  oDBResultSet := Execute;
  iID := -1;
  while oDBResultSet.NotEof do
  begin
    if iID <> VarAsType(oDBResultSet.GetFieldValue('id'), varInteger) then
    begin
      oForeignKey := TmkForeignKey.Create(ATable);
      with oForeignKey do
      begin
        Name := Format('fk_%s_%s', [VarToStr(oDBResultSet.GetFieldValue('id')), VarToStr(oDBResultSet.GetFieldValue('table'))]);
        ToTable := TmkTable.Create(FCatalogMetadata);
        ToTable.Name := VarToStr(oDBResultSet.GetFieldValue('table'));
        OnUpdate := TRttiSingleton.GetInstance.GetRuleAction(VarToStr(oDBResultSet.GetFieldValue('on_update')));
        OnDelete := TRttiSingleton.GetInstance.GetRuleAction(VarToStr(oDBResultSet.GetFieldValue('on_delete')));
        iID := VarAsType(oDBResultSet.GetFieldValue('id'), varInteger);
      end;
      ATable.ForeignKeys.Add(oForeignKey);
    end;
    ///
    oFromField := TmkTableField.Create(ATable);
    oFromField.Name := VarToStr(oDBResultSet.GetFieldValue('from'));
    oForeignKey.FromFields.Add(oFromField);
    ///
    oToField := TmkTableField.Create(oForeignKey.ToTable);
    oToField.Name := VarToStr(oDBResultSet.GetFieldValue('to'));
    oForeignKey.ToFields.Add(oFromField);
  end;
end;

procedure TCatalogMetadataSQLite.GetFunctions;
begin
  inherited;

end;

procedure TCatalogMetadataSQLite.GetProcedures;
begin
  inherited;

end;

procedure TCatalogMetadataSQLite.GetSequences;
begin
  inherited;

end;

procedure TCatalogMetadataSQLite.GetTriggers(ATable: TmkTable);
begin
  inherited;

end;

procedure TCatalogMetadataSQLite.GetIndexeKeys(ATable: TmkTable);
var
  oDBResultSet: IDBResultSet;
  oIndexeKey: TmkIndexeKey;
begin
  inherited;
  FSQL := Format('pragma index_list("%s")', [ATable.Name]);
  oDBResultSet := Execute;
  while oDBResultSet.NotEof do
  begin
    if VarToStr(oDBResultSet.GetFieldValue('origin')) = 'pk' then
       Continue;
    oIndexeKey := oIndexeKey.Create(ATable);
    with oIndexeKey do
    begin
      Name := VarToStr(oDBResultSet.GetFieldValue('name'));
      Unique := VarAsType(oDBResultSet.GetFieldValue('notnull'), varInteger) = 1;
//      AutoIncrement: boolean;
    end;
    ATable.IndexeKeys.Add(oIndexeKey);
  end;
end;

procedure TCatalogMetadataSQLite.GetViews;
begin
  inherited;

end;

initialization
  TMetadataRegister.RegisterMetadata(dnSQLite, TCatalogMetadataSQLite.Create);

end.
