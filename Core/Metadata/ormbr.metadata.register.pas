{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.metadata.register;

interface

uses
  Generics.Collections,
  ormbr.metadata.extract,
  ormbr.factory.interfaces;

type
  TMetadataRegister = class
  strict private
    class var FSchema: TObjectDictionary<TDriverName,TCatalogMetadataAbstract>;
  private
    class constructor Create;
    class destructor Destroy;
  public
    class procedure RegisterMetadata(const ADriverName: TDriverName; const ACatalogMetadata: TCatalogMetadataAbstract);
    class function GetMetadata(const ADriverName: TDriverName): TCatalogMetadataAbstract;
  end;

implementation

class constructor TMetadataRegister.Create;
begin
  FSchema := TObjectDictionary<TDriverName,TCatalogMetadataAbstract>.Create([doOwnsValues]);
end;

class destructor TMetadataRegister.Destroy;
begin
  FSchema.Free;
  inherited;
end;

class function TMetadataRegister.GetMetadata(const ADriverName: TDriverName): TCatalogMetadataAbstract;
begin
  Result := FSchema[ADriverName];
end;

class procedure TMetadataRegister.RegisterMetadata(const ADriverName: TDriverName; const ACatalogMetadata: TCatalogMetadataAbstract);
begin
  FSchema.AddOrSetValue(ADriverName, ACatalogMetadata);
end;

end.

