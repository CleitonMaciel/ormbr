{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.metadata.model;

interface

uses
  ormbr.metadata.extract,
  ormbr.database.mapping,
  ormbr.mapping.explorer;

type
  TModelMetadata = class(TModelMetadataAbstract)
  protected
    procedure GetCatalogs; override;
    procedure GetSchemas; override;
    procedure GetTables; override;
    procedure GetColumns(ATableName: string); override;
    procedure GetPrimaryKey; override;
    procedure GetIndexeKeys; override;
    procedure GetForeignKeys; override;
    procedure GetSequences; override;
    procedure GetProcedures; override;
    procedure GetFunctions; override;
    procedure GetViews; override;
    procedure GetTriggers; override;
    procedure GetModelMetadata; override;
  end;

implementation

{ TModelMetadata }

procedure TModelMetadata.GetModelMetadata;
begin
  inherited;
  GetCatalogs;
end;

procedure TModelMetadata.GetCatalogs;
begin
  inherited;
  GetSchemas;
end;

procedure TModelMetadata.GetSchemas;
begin
  inherited;
  GetTables;
end;

procedure TModelMetadata.GetTables;
var
  oClass: TClass;
begin
  inherited;
  for oClass in TMappingExplorer.GetInstance.Repository.List do
  begin
//    FCatalogMetadata.Tables.Add(TMappingExplorer.GetInstance.GetMappingTable(oClass));
  end;
end;

procedure TModelMetadata.GetColumns(ATableName: string);
begin
  inherited;

end;

procedure TModelMetadata.GetForeignKeys;
begin
  inherited;

end;

procedure TModelMetadata.GetFunctions;
begin
  inherited;

end;

procedure TModelMetadata.GetPrimaryKey;
begin
  inherited;

end;

procedure TModelMetadata.GetProcedures;
begin
  inherited;

end;

procedure TModelMetadata.GetSequences;
begin
  inherited;

end;

procedure TModelMetadata.GetTriggers;
begin
  inherited;

end;

procedure TModelMetadata.GetIndexeKeys;
begin
  inherited;

end;

procedure TModelMetadata.GetViews;
begin
  inherited;

end;

end.
