{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.interfaces;

interface

uses
  DB,
  Rtti,
  GpSQLBuilder,
  Generics.Collections,
  ormbr.factory.interfaces;

type
  /// <summary>
  /// Interface de conex�o
  /// </summary>
  IBuildSQLCommand = interface
    ['{DC88F075-82C4-4573-B0C3-CDF96C47A1A7}']
    procedure BuildSQLUpdate(AObject: TObject); overload;
    procedure BuildSQLUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>); overload;
    procedure BuildSQLInsert(AObject: TObject);
    procedure BuildSQLDelete(AObject: TObject);
    function BuildSQLSelectAll(AClass: TClass; APageSize: Integer): IDBResultSet;
    function BuildSQLSelectID(AClass: TClass; AID: TValue; APageSize: Integer): IDBResultSet;
    function BuildSQLSelect(ASQL: IGpSQLBuilder; APageSize: Integer): IDBResultSet;
    function BuildSQLNextPacket: IDBResultSet;
    function BuildSQLSequence(AClass: TClass): IDBResultSet;
  end;

implementation

end.
