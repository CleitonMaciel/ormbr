{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.updater;

interface

uses
  DB,
  Generics.Collections,
  ormbr.command.base,
  ormbr.factory.interfaces;

type
  TCommandUpdater = class(TCommandBase)
  public
    procedure GenerateUpdate(AObject: TObject); overload;
    procedure GenerateUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>); overload;
    procedure ExecuteUpdate;
  end;

implementation

{ TCommandUpdater }

procedure TCommandUpdater.GenerateUpdate(AObject: TObject);
begin
  FSQL := FDriverCommand.BuildSQLUpdate(AObject);
end;

procedure TCommandUpdater.GenerateUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>);
begin
  FSQL := FDriverCommand.BuildSQLUpdate(AObject,AModifiedFields);
end;

procedure TCommandUpdater.ExecuteUpdate;
begin
  ExecuteDirect;
end;

end.
