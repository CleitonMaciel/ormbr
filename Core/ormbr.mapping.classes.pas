{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.mapping.classes;

interface

uses
  DB,
  Generics.Collections,
  TypInfo,
  Rtti,
  ormbr.mapping.attributes,
  ormbr.types.mapping;

type
  TMappingDescription = class abstract
  protected
    FDescription: string;
  public
    property Description: string read FDescription write FDescription;
  end;

  TColumnBase = class(TMappingDescription)
  private
    FFieldIndex: Integer;
    FName: string;
    FFieldType: TFieldType;
  public
    constructor Create(AFieldIndex: Integer; AName: string); overload;
    constructor Create(AFieldIndex: Integer; AName: string; AFieldType: TFieldType); overload;
    property FieldIndex: Integer read FFieldIndex write FFieldIndex;
    property Name: string read FName write FName;
    property FieldType: TFieldType read FFieldType write FFieldType;
  end;

  /// TableMapping
  TTableMapping = class(TMappingDescription)
  private
    FName: string;
    FSchema: string;
  public
    property Name: string read FName write FName;
    property Schema: string read FSchema write FSchema;
  end;

  /// ColumnMapping
  TColumnMapping = class(TColumnBase)
  private
    FSize: Integer;
    FPrecision: Integer;
    FScale: Integer;
  public
    property Size: Integer read FSize write FSize;
    property Precision: Integer read FPrecision write FPrecision;
    property Scale: Integer read FScale write FScale;
  end;
  /// ColumnMappingList
  TColumnMappingList = class(TObjectList<TColumnMapping>);

  /// AssociationMapping
  TAssociationMapping = class
  private
    FMultiplicity: TMultiplicity;
    FColumnName: string;
    FReferencedColumnName: string;
    FReferencedClassName: string;
    FColumns: TArray<string>;
  public
    constructor Create(AMultiplicity: TMultiplicity; AColumnName, AReferencedColumnName, AReferencedClassName: string); overload;
    constructor Create(AMultiplicity: TMultiplicity; AColumnName, AReferencedColumnName, AReferencedClassName: string; AColumns: array of string); overload;
    property Multiplicity: TMultiplicity read FMultiplicity;
    property ColumnName: string read FColumnName;
    property ReferencedColumnName: string read FReferencedColumnName;
    property ReferencedClassName: string read FReferencedClassName;
    property Columns: TArray<string> read FColumns;
  end;
  /// AssociationMappingList
  TAssociationMappingList = class(TObjectList<TAssociationMapping>);

  /// IndexeMapping
  TIndexeMapping = class(TMappingDescription)
  private
    FName: string;
    FColumns: TArray<string>;
    FSortingOrder: TSortingOrder;
    FUnique: Boolean;
  public
    constructor Create(AName: string; AColumns: array of string; ASortingOrder: TSortingOrder = Ascending; AUnique: Boolean = False; ADescription: string = '');
    property Name: string read FName write FName;
    property Columns: TArray<string> read FColumns;
    property SortingOrder: TSortingOrder read FSortingOrder write FSortingOrder;
    property Unique: Boolean read FUnique write FUnique;
  end;
  /// IndexeMappingList
  TIndexeMappingList = class(TObjectList<TIndexeMapping>);

  /// PrimaryKeyMapping
  TPrimaryKeyMapping = class(TMappingDescription)
  private
    FColumnName: string;
    FSortingOrder: TSortingOrder;
  public
    constructor Create(AColumnName: string; ASortingOrder: TSortingOrder; ADescription: string = '');
    property ColumnName: string read FColumnName write FColumnName;
    property SortingOrder: TSortingOrder read FSortingOrder;
  end;
  /// PrimaryKeyMappingList
  TPrimaryKeyMappingList = class(TObjectList<TPrimaryKeyMapping>);

  /// ForeignKeyKeyMapping
  TForeignKeyMapping = class(TMappingDescription)
  private
    FReferenceTableName: string;
    FRuleUpdate: TRuleAction;
    FRuleDelete: TRuleAction;
  public
    constructor Create(AReferenceTableName: string; ARuleDelete, ARuleUpdate: TRuleAction; ADescription: string = '');
    property ReferenceTableName: string read FReferenceTableName;
    property RuleDelete: TRuleAction read FRuleDelete;
    property RuleUpdate: TRuleAction read FRuleUpdate;
  end;
  /// ForeignKeyMappingList
  TForeignKeyMappingList = class(TObjectList<TForeignKeyMapping>);

  /// JoinColumnMapping
  TJoinColumnMapping = class
  private
    FColumnName: string;
    FReferencedTableName: string;
    FReferencedColumnName: string;
    FJoin: TJoin;
  public
    constructor Create(AColumnName, AReferencedTableName, AReferencedColumnName: string; AJoin: TJoin = InnerJoin);
    property ColumnName: string read FColumnName;
    property ReferencedColumnName: string read FReferencedColumnName;
    property ReferencedTableName: string read FReferencedTableName;
    property Join: TJoin read FJoin write FJoin;
  end;
  /// AssociationMappingList
  TJoinColumnMappingList = class(TObjectList<TJoinColumnMapping>);

implementation

{ TIdColumnMapping }

constructor TColumnBase.Create(AFieldIndex: Integer; AName: string);
begin
  FFieldIndex := AFieldIndex;
  FName := AName;
end;

constructor TColumnBase.Create(AFieldIndex: Integer; AName: string; AFieldType: TFieldType);
begin
  Create(AFieldIndex, AName);
  FFieldType := AFieldType;
end;

{ TOneToOneRelationMapping }

constructor TAssociationMapping.Create(AMultiplicity: TMultiplicity; AColumnName,
  AReferencedColumnName, AReferencedClassName: string);
begin
  FMultiplicity := AMultiplicity;
  FColumnName := AColumnName;
  FReferencedColumnName := AReferencedColumnName;
  FReferencedClassName := AReferencedClassName;
end;

constructor TAssociationMapping.Create(AMultiplicity: TMultiplicity; AColumnName,
  AReferencedColumnName, AReferencedClassName: string; AColumns: array of string);
var
  iFor: Integer;
begin
  Create(AMultiplicity, AColumnName, AReferencedColumnName, AReferencedClassName);
  if Length(AColumns) > 0 then
  begin
    SetLength(FColumns, Length(AColumns));
    for iFor := Low(AColumns) to High(AColumns) do
      FColumns[iFor] := AColumns[iFor];
  end;
end;

{ TIndexMapping }

constructor TIndexeMapping.Create(AName: string; AColumns: array of string;
  ASortingOrder: TSortingOrder; AUnique: Boolean; ADescription: string);
var
  iFor: Integer;
begin
  FName := AName;
  FSortingOrder := ASortingOrder;
  FUnique := AUnique;
  FDescription := ADescription;
  if Length(AColumns) > 0 then
  begin
    SetLength(FColumns, Length(AColumns));
    for iFor := Low(AColumns) to High(AColumns) do
      FColumns[iFor] := AColumns[iFor];
  end;
end;

{ TJoinColumnMapping }

constructor TJoinColumnMapping.Create(AColumnName, AReferencedTableName, AReferencedColumnName: string; AJoin: TJoin);
begin
  FColumnName := AColumnName;
  FReferencedTableName := AReferencedTableName;
  FReferencedColumnName := AReferencedColumnName;
  FJoin := AJoin;
end;

{ TPrimaryKeyMapping }

constructor TPrimaryKeyMapping.Create(AColumnName: string; ASortingOrder: TSortingOrder; ADescription: string);
begin
  FColumnName := AColumnName;
  FSortingOrder := ASortingOrder;
  FDescription := ADescription;
end;

{ TForeignKeyMapping }

constructor TForeignKeyMapping.Create(AReferenceTableName: string; ARuleDelete,
  ARuleUpdate: TRuleAction; ADescription: string);
begin
  FReferenceTableName := AReferenceTableName;
  FRuleDelete := ARuleDelete;
  FRuleUpdate := ARuleUpdate;
  FDescription := ADescription;
end;

end.
