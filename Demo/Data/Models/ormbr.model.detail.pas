unit ormbr.model.detail;

interface

uses
  Classes, 
  DB, 
  SysUtils, 
  Generics.Collections, 
  /// orm 
  ormbr.mapping.attributes,
  ormbr.types.mapping,
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.lazy,
  ormbr.model.lookup;

type
  [Entity]
  [Table('detail','')]
  Tdetail = class
  private
    { Private declarations }
    Fdetail_id: Integer;
    Fmaster_id: Integer;
    Flookup_id: Integer;
    Flookup_description: String;
    Fprice: Currency;

    Flookup: Tlookup;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;

    [PrimaryKey()]
    [Column('detail_id', ftInteger)]
    [Dictionary('detail_id','Mensagem de valida��o','','','',taCenter)]
    property detail_id: Integer Index 0 read Fdetail_id write Fdetail_id;

    [Column('master_id', ftInteger)]
    [Dictionary('master_id','Mensagem de valida��o','','','',taCenter)]
    property master_id: Integer Index 1 read Fmaster_id write Fmaster_id;

    [Column('lookup_id', ftInteger)]
    [Dictionary('lookup_id','Mensagem de valida��o','','','',taCenter)]
    property lookup_id: Integer Index 2 read Flookup_id write Flookup_id;

    [Column('lookup_description', ftString)]
    [Dictionary('lookup_description','Mensagem de valida��o','','','',taLeftJustify)]
    property lookup_description: String Index 3 read Flookup_description write Flookup_description;

    [Column('price', ftCurrency)]
    [Dictionary('price','Mensagem de valida��o','0','','',taRightJustify)]
    property price: Currency Index 4 read Fprice write Fprice;

    [ForeignKey('Lookup', None, None)]
    property lookup: Tlookup read Flookup write Flookup;
  end;

implementation

{ Tdetail }

constructor Tdetail.Create;
begin
   Flookup := Tlookup.Create;
end;

destructor Tdetail.Destroy;
begin
  Flookup.Free;
  inherited;
end;

end.
