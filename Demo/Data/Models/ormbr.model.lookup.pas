unit ormbr.model.lookup;

interface

uses
  Classes, 
  DB, 
  SysUtils, 
  Generics.Collections, 
  /// orm 
  ormbr.mapping.attributes, 
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.lazy;

type
  [Entity]
  [Table('lookup','')]
  Tlookup = class
  private
    { Private declarations } 
    Flookup_id: Integer;
    Flookup_description: String;
  public
    { Public declarations }
    [PrimaryKey()]
    [Column('lookup_id', ftInteger)]
    [Dictionary('lookup_id','Mensagem de valida��o','','','',taCenter)]
    property lookup_id: Integer Index 0 read Flookup_id write Flookup_id;

    [Column('lookup_description', ftString, 60)]
    [Indexe('idx_lookup_description','lookup_description')]
    [Dictionary('lookup_description','Mensagem de valida��o','','','',taLeftJustify)]
    property lookup_description: String Index 1 read Flookup_description write Flookup_description;
  end;

implementation

end.
