program DelphiORMBr;

uses
  Forms,
  SysUtils,
  uMainFormORM in 'uMainFormORM.pas' {Form3},
  GpSQLBuilder.AST in '..\..\..\Core\GpSQLBuilder.AST.pas',
  GpSQLBuilder in '..\..\..\Core\GpSQLBuilder.pas',
  GpSQLBuilder.Serialize in '..\..\..\Core\GpSQLBuilder.Serialize.pas',
  ormbr.bind.base in '..\..\..\Core\ormbr.bind.base.pas',
  ormbr.bind.clientdataset in '..\..\..\Core\ormbr.bind.clientdataset.pas',
  ormbr.bind.events in '..\..\..\Core\ormbr.bind.events.pas',
  ormbr.bind.fields in '..\..\..\Core\ormbr.bind.fields.pas',
  ormbr.bind.mapping in '..\..\..\Core\ormbr.bind.mapping.pas',
  ormbr.command.base in '..\..\..\Core\ormbr.command.base.pas',
  ormbr.command.deleter in '..\..\..\Core\ormbr.command.deleter.pas',
  ormbr.command.inserter in '..\..\..\Core\ormbr.command.inserter.pas',
  ormbr.command.interfaces in '..\..\..\Core\ormbr.command.interfaces.pas',
  ormbr.command.selecter in '..\..\..\Core\ormbr.command.selecter.pas',
  ormbr.command.updater in '..\..\..\Core\ormbr.command.updater.pas',
  ormbr.criteria.interfaces in '..\..\..\Core\ormbr.criteria.interfaces.pas',
  ormbr.dependency.interfaces in '..\..\..\Core\ormbr.dependency.interfaces.pas',
  ormbr.driver.command.firebird in '..\..\..\Core\ormbr.driver.command.firebird.pas',
  ormbr.driver.command.mssql in '..\..\..\Core\ormbr.driver.command.mssql.pas',
  ormbr.driver.command.mysql in '..\..\..\Core\ormbr.driver.command.mysql.pas',
  ormbr.driver.command in '..\..\..\Core\ormbr.driver.command.pas',
  ormbr.driver.command.sqlite in '..\..\..\Core\ormbr.driver.command.sqlite.pas',
  ormbr.driver.connection in '..\..\..\Drivers\ormbr.driver.connection.pas',
  ormbr.factory.connection in '..\..\..\Drivers\ormbr.factory.connection.pas',
  ormbr.factory.interfaces in '..\..\..\Drivers\ormbr.factory.interfaces.pas',
  ormbr.types.nullable in '..\..\..\Core\ormbr.types.nullable.pas',
  ormbr.types.lazy in '..\..\..\Core\ormbr.types.lazy.pas',
  ormbr.driver.firedac in '..\..\..\Drivers\ormbr.driver.firedac.pas',
  ormbr.driver.firedac.transaction in '..\..\..\Drivers\ormbr.driver.firedac.transaction.pas',
  ormbr.factory.firedac in '..\..\..\Drivers\ormbr.factory.firedac.pas',
  ormbr.model.client in '..\Models\ormbr.model.client.pas',
  ormbr.model.detail in '..\Models\ormbr.model.detail.pas',
  ormbr.model.lookup in '..\Models\ormbr.model.lookup.pas',
  ormbr.model.master in '..\Models\ormbr.model.master.pas',
  ormbr.command.factory in '..\..\..\Core\ormbr.command.factory.pas',
  ormbr.session.manager in '..\..\..\Core\ormbr.session.manager.pas',
  ormbr.dependency.injection.clientdataset in '..\..\..\Core\ormbr.dependency.injection.clientdataset.pas',
  ormbr.dependency.injection.fdmemtable in '..\..\..\Core\ormbr.dependency.injection.fdmemtable.pas',
  ormbr.objects.manager in '..\..\..\Core\ormbr.objects.manager.pas',
  ormbr.driver.register in '..\..\..\Core\ormbr.driver.register.pas',
  ormbr.bind.fdmemtable in '..\..\..\Core\ormbr.bind.fdmemtable.pas',
  ormbr.types.mapping in '..\..\..\Core\ormbr.types.mapping.pas',
  ormbr.mapping.attributes in '..\..\..\Core\ormbr.mapping.attributes.pas',
  ormbr.mapping.classes in '..\..\..\Core\ormbr.mapping.classes.pas',
  ormbr.mapping.exceptions in '..\..\..\Core\ormbr.mapping.exceptions.pas',
  ormbr.mapping.explorer in '..\..\..\Core\ormbr.mapping.explorer.pas',
  ormbr.mapping.explorerstrategy in '..\..\..\Core\ormbr.mapping.explorerstrategy.pas',
  ormbr.mapping.helper in '..\..\..\Core\ormbr.mapping.helper.pas',
  ormbr.mapping.popular in '..\..\..\Core\ormbr.mapping.popular.pas',
  ormbr.mapping.register in '..\..\..\Core\ormbr.mapping.register.pas',
  ormbr.mapping.repository in '..\..\..\Core\ormbr.mapping.repository.pas',
  ormbr.mapping.rttiutils in '..\..\..\Core\ormbr.mapping.rttiutils.pas',
  ormbr.controller.base in '..\..\..\Core\Controller\ormbr.controller.base.pas',
  ormbr.controller.abstract in '..\..\..\Core\Controller\ormbr.controller.abstract.pas';

{$R *.res}

begin
  Application.Initialize;
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
