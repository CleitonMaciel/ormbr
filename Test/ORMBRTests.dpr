program ORMBRTests;

{$IFNDEF TESTINSIGHT}
{$APPTYPE CONSOLE}
{$ENDIF}{$STRONGLINKTYPES ON}
uses
  System.SysUtils,
  {$IFDEF TESTINSIGHT}
  TestInsight.DUnitX,
  {$ENDIF }
  DUnitX.Loggers.Console,
  DUnitX.Loggers.Xml.NUnit,
  DUnitX.TestFramework,
  GpSQLBuilder.AST in '..\Core\GpSQLBuilder.AST.pas',
  GpSQLBuilder in '..\Core\GpSQLBuilder.pas',
  GpSQLBuilder.Serialize in '..\Core\GpSQLBuilder.Serialize.pas',
  ormbr.bind.base in '..\Core\ormbr.bind.base.pas',
  ormbr.bind.clientdataset in '..\Core\ormbr.bind.clientdataset.pas',
  ormbr.bind.events in '..\Core\ormbr.bind.events.pas',
  ormbr.bind.fdmemtable in '..\Core\ormbr.bind.fdmemtable.pas',
  ormbr.bind.fields in '..\Core\ormbr.bind.fields.pas',
  ormbr.bind.mapping in '..\Core\ormbr.bind.mapping.pas',
  ormbr.command.base in '..\Core\ormbr.command.base.pas',
  ormbr.command.deleter in '..\Core\ormbr.command.deleter.pas',
  ormbr.command.factory in '..\Core\ormbr.command.factory.pas',
  ormbr.command.inserter in '..\Core\ormbr.command.inserter.pas',
  ormbr.command.interfaces in '..\Core\ormbr.command.interfaces.pas',
  ormbr.command.selecter in '..\Core\ormbr.command.selecter.pas',
  ormbr.command.updater in '..\Core\ormbr.command.updater.pas',
  ormbr.criteria.interfaces in '..\Core\ormbr.criteria.interfaces.pas',
  ormbr.dependency.injection.clientdataset in '..\Core\ormbr.dependency.injection.clientdataset.pas',
  ormbr.dependency.injection.fdmemtable in '..\Core\ormbr.dependency.injection.fdmemtable.pas',
  ormbr.dependency.interfaces in '..\Core\ormbr.dependency.interfaces.pas',
  ormbr.driver.command.firebird in '..\Core\ormbr.driver.command.firebird.pas',
  ormbr.driver.command.mssql in '..\Core\ormbr.driver.command.mssql.pas',
  ormbr.driver.command.mysql in '..\Core\ormbr.driver.command.mysql.pas',
  ormbr.driver.command in '..\Core\ormbr.driver.command.pas',
  ormbr.driver.command.sqlite in '..\Core\ormbr.driver.command.sqlite.pas',
  ormbr.driver.register in '..\Core\ormbr.driver.register.pas',
  ormbr.mapping.attributes in '..\Core\ormbr.mapping.attributes.pas',
  ormbr.mapping.classes in '..\Core\ormbr.mapping.classes.pas',
  ormbr.mapping.exceptions in '..\Core\ormbr.mapping.exceptions.pas',
  ormbr.mapping.explorer in '..\Core\ormbr.mapping.explorer.pas',
  ormbr.mapping.explorerstrategy in '..\Core\ormbr.mapping.explorerstrategy.pas',
  ormbr.mapping.helper in '..\Core\ormbr.mapping.helper.pas',
  ormbr.mapping.popular in '..\Core\ormbr.mapping.popular.pas',
  ormbr.mapping.register in '..\Core\ormbr.mapping.register.pas',
  ormbr.mapping.repository in '..\Core\ormbr.mapping.repository.pas',
  ormbr.mapping.rttiutils in '..\Core\ormbr.mapping.rttiutils.pas',
  ormbr.objects.manager in '..\Core\ormbr.objects.manager.pas',
  ormbr.session.manager in '..\Core\ormbr.session.manager.pas',
  ormbr.types.lazy in '..\Core\ormbr.types.lazy.pas',
  ormbr.types.mapping in '..\Core\ormbr.types.mapping.pas',
  ormbr.types.nullable in '..\Core\ormbr.types.nullable.pas',
  ormbr.controller.abstract in '..\Core\Controller\ormbr.controller.abstract.pas',
  ormbr.controller.base in '..\Core\Controller\ormbr.controller.base.pas',
  ormbr.database.abstract in '..\Core\Metadata\ormbr.database.abstract.pas',
  ormbr.database.manager in '..\Core\Metadata\ormbr.database.manager.pas',
  ormbr.database.mapping in '..\Core\Metadata\ormbr.database.mapping.pas',
  ormbr.metadata.classe.factory in '..\Core\Metadata\ormbr.metadata.classe.factory.pas',
  ormbr.metadata.db.factory in '..\Core\Metadata\ormbr.metadata.db.factory.pas',
  ormbr.metadata.extract in '..\Core\Metadata\ormbr.metadata.extract.pas',
  ormbr.metadata.firebird in '..\Core\Metadata\ormbr.metadata.firebird.pas',
  ormbr.metadata.interfaces in '..\Core\Metadata\ormbr.metadata.interfaces.pas',
  ormbr.metadata.model in '..\Core\Metadata\ormbr.metadata.model.pas',
  ormbr.metadata.register in '..\Core\Metadata\ormbr.metadata.register.pas',
  ormbr.metadata.sqlite in '..\Core\Metadata\ormbr.metadata.sqlite.pas',
  FactoryDBExpressTests in 'FactoryDBExpressTests.pas',
  FactoryZeosTests in 'FactoryZeosTests.pas',
  ormbr.driver.connection in '..\Drivers\ormbr.driver.connection.pas',
  ormbr.driver.dbexpress in '..\Drivers\ormbr.driver.dbexpress.pas',
  ormbr.driver.dbexpress.transaction in '..\Drivers\ormbr.driver.dbexpress.transaction.pas',
  ormbr.driver.firedac in '..\Drivers\ormbr.driver.firedac.pas',
  ormbr.driver.firedac.transaction in '..\Drivers\ormbr.driver.firedac.transaction.pas',
  ormbr.driver.zeos in '..\Drivers\ormbr.driver.zeos.pas',
  ormbr.driver.zeos.transaction in '..\Drivers\ormbr.driver.zeos.transaction.pas',
  ormbr.factory.connection in '..\Drivers\ormbr.factory.connection.pas',
  ormbr.factory.dbexpress in '..\Drivers\ormbr.factory.dbexpress.pas',
  ormbr.factory.firedac in '..\Drivers\ormbr.factory.firedac.pas',
  ormbr.factory.interfaces in '..\Drivers\ormbr.factory.interfaces.pas',
  ormbr.factory.zeos in '..\Drivers\ormbr.factory.zeos.pas';

var
  runner : ITestRunner;
  results : IRunResults;
  logger : ITestLogger;
  nunitLogger : ITestLogger;
begin
{$IFDEF TESTINSIGHT}
  TestInsight.DUnitX.RunRegisteredTests;
  exit;
{$ENDIF}
  try
    //Check command line options, will exit if invalid
    TDUnitX.CheckCommandLine;
    //Create the test runner
    runner := TDUnitX.CreateRunner;
    //Tell the runner to use RTTI to find Fixtures
    runner.UseRTTI := True;
    //tell the runner how we will log things
    //Log to the console window
    logger := TDUnitXConsoleLogger.Create(true);
    runner.AddLogger(logger);
    //Generate an NUnit compatible XML File
    nunitLogger := TDUnitXXMLNUnitFileLogger.Create(TDUnitX.Options.XMLOutputFile);
    runner.AddLogger(nunitLogger);
    runner.FailsOnNoAsserts := False; //When true, Assertions must be made during tests;

    //Run tests
    results := runner.Execute;
    if not results.AllPassed then
      System.ExitCode := EXIT_ERRORS;

    {$IFNDEF CI}
    //We don't want this happening when running under CI.
    if TDUnitX.Options.ExitBehavior = TDUnitXExitBehavior.Pause then
    begin
      System.Write('Done.. press <Enter> key to quit.');
      System.Readln;
    end;
    {$ENDIF}
  except
    on E: Exception do
      System.Writeln(E.ClassName, ': ', E.Message);
  end;
end.
