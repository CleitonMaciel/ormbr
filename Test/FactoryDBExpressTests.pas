unit FactoryDBExpressTests;

interface
uses
  DUnitX.TestFramework,
  Data.SqlExpr,
  Data.DbxSqlite,
  Data.DB,
  System.SysUtils,
  ormbr.factory.interfaces,
  ormbr.factory.dbexpress;

type

  [TestFixture]
  TFactoryDBExpressTests = class(TObject)
  private
    FSQLConnection: TSQLConnection;
    FConnection   : IDBConnection;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure FactoryDBExpressIDBConnectionConnectTest;
    [Test]
    procedure FactoryDBExpressIDBConnectionDisconnectTest;
    [Test]
    procedure FactoryDBExpressIDBConnectionAndTSQLConnectionConnectTest;
    [Test]
    procedure FactoryDBExpressIDBConnectionAndTSQLConnectionDisconnectTest;
    [Test]
    procedure FactoryDBExpressInTransactionTest;
    [Test]
    procedure FactoryDBExpressIDBConnectionAndTSQLConnectionInTransactionTest;
  end;

implementation

procedure TFactoryDBExpressTests.FactoryDBExpressInTransactionTest;
begin
  FConnection.Connect;
  FConnection.StartTransaction;
  Assert.IsTrue(FConnection.InTransaction, 'FConnection.InTransaction = false');
end;

procedure TFactoryDBExpressTests.Setup;
begin
  FSQLConnection := TSQLConnection.Create(nil);
  FSQLConnection.KeepConnection := False;
  FSQLConnection.LoadParamsOnConnect := False;
  FSQLConnection.LoginPrompt := False;
  FSQLConnection.Params.Clear;
  FSQLConnection.DriverName := 'Sqlite';
  FSQLConnection.Params.Add('Database=..\Demo\Data\Database\database.db3');
  FSQLConnection.Params.Add('FailIfMissing=True');

  FConnection := TFactoryDBExpress.Create(FSQLConnection, dnSQLite);
end;

procedure TFactoryDBExpressTests.TearDown;
begin
  if Assigned(FSQLConnection) then
    FreeAndNil(FSQLConnection);
end;

procedure TFactoryDBExpressTests.FactoryDBExpressIDBConnectionAndTSQLConnectionConnectTest;
begin
  FConnection.Connect;
  Assert.AreEqual(FConnection.IsConnected, FSQLConnection.Connected, 'FConnection.IsConnected <> FSQLConnection.Connected');
end;

procedure TFactoryDBExpressTests.FactoryDBExpressIDBConnectionAndTSQLConnectionDisconnectTest;
begin
  FConnection.Connect;
  FConnection.Disconnect;
  Assert.AreEqual(FConnection.IsConnected, FSQLConnection.Connected, 'FConnection.IsConnected <> FSQLConnection.Connected');
end;

procedure TFactoryDBExpressTests.FactoryDBExpressIDBConnectionAndTSQLConnectionInTransactionTest;
begin
  FConnection.Connect;
  FConnection.StartTransaction;
  Assert.AreEqual(FConnection.InTransaction, FSQLConnection.InTransaction, 'FConnection.InTransaction <> FSQLConnection.InTransaction');
end;

procedure TFactoryDBExpressTests.FactoryDBExpressIDBConnectionConnectTest;
begin
  FConnection.Connect;
  Assert.IsTrue(FConnection.IsConnected, 'FConnection.IsConnected = false');
end;


procedure TFactoryDBExpressTests.FactoryDBExpressIDBConnectionDisconnectTest;
begin
  FConnection.Connect;
  FConnection.Disconnect;
  Assert.IsFalse(FConnection.IsConnected, 'FConnection.IsConnected = true');
end;

initialization
  TDUnitX.RegisterTestFixture(TFactoryDBExpressTests);
end.
