unit FactoryZeosTests;

interface
uses
  DUnitX.TestFramework,
  Data.DbxSqlite,
  Data.DB,
  System.SysUtils,
  ZConnection,
  ormbr.factory.interfaces,
  ormbr.factory.zeos;

type

  [TestFixture]
  TFactoryZeosTests = class(TObject)
  private
    FZConnection: TZConnection;
    FConnection : IDBConnection;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure FactoryZeosIDBConnectionConnectTest;
    [Test]
    procedure FactoryZeosIDBConnectionDisconnectTest;
    [Test]
    procedure FactoryZeosIDBConnectionAndTZConnectionConnectTest;
    [Test]
    procedure FactoryZeosIDBConnectionAndTZConnectionDisconnectTest;
    [Test]
    procedure FactoryZeosInTransactionTest;
    [Test]
    procedure FactoryZeosIDBConnectionAndTZConnectionInTransactionTest;
  end;

implementation

procedure TFactoryZeosTests.FactoryZeosInTransactionTest;
begin
  FConnection.Connect;
  FConnection.StartTransaction;
  Assert.IsTrue(FConnection.InTransaction, 'FConnection.InTransaction = false');
end;

procedure TFactoryZeosTests.Setup;
begin
  FZConnection := TZConnection.Create(nil);
  FZConnection.LoginPrompt := False;
  FZConnection.Protocol := 'sqlite-3';
  FZConnection.Database := '..\Demo\Data\Database\database.db3';

  FConnection := TFactoryZeos.Create(FZConnection, dnSQLite);
end;

procedure TFactoryZeosTests.TearDown;
begin
  if Assigned(FZConnection) then
    FreeAndNil(FZConnection);
end;

procedure TFactoryZeosTests.FactoryZeosIDBConnectionAndTZConnectionConnectTest;
begin
  FConnection.Connect;
  Assert.AreEqual(FConnection.IsConnected, FZConnection.Connected, 'FConnection.IsConnected <> FSQLConnection.Connected');
end;

procedure TFactoryZeosTests.FactoryZeosIDBConnectionAndTZConnectionDisconnectTest;
begin
  FConnection.Connect;
  FConnection.Disconnect;
  Assert.AreEqual(FConnection.IsConnected, FZConnection.Connected, 'FConnection.IsConnected <> FSQLConnection.Connected');
end;

procedure TFactoryZeosTests.FactoryZeosIDBConnectionAndTZConnectionInTransactionTest;
begin
  FConnection.Connect;
  FConnection.StartTransaction;
  Assert.AreEqual(FConnection.InTransaction, FZConnection.InTransaction, 'FConnection.InTransaction <> FSQLConnection.InTransaction');
end;

procedure TFactoryZeosTests.FactoryZeosIDBConnectionConnectTest;
begin
  FConnection.Connect;
  Assert.IsTrue(FConnection.IsConnected, 'FConnection.IsConnected = false');
end;


procedure TFactoryZeosTests.FactoryZeosIDBConnectionDisconnectTest;
begin
  FConnection.Connect;
  FConnection.Disconnect;
  Assert.IsFalse(FConnection.IsConnected, 'FConnection.IsConnected = true');
end;

initialization
  TDUnitX.RegisterTestFixture(TFactoryZeosTests);

end.
