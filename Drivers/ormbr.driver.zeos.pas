{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.driver.zeos;

interface

uses
  Classes,
  DB,
  Variants,
  ZAbstractConnection,
  ZConnection,
  ZAbstractRODataset,
  ZAbstractDataset,
  ZDataset,
  /// orm
  ormbr.driver.connection,
  ormbr.factory.interfaces;

type
  /// <summary>
  /// Classe de conex�o concreta com dbExpress
  /// </summary>
  TDriverZeos = class(TDriverConnection)
  protected
    FConnection: TZConnection;
  public
    constructor Create(AConnection: TComponent; ADriverName: TDriverName); override;
    destructor Destroy; override;
    procedure Connect; override;
    procedure Disconnect; override;
    function IsConnected: Boolean; override;
    function InTransaction: Boolean; override;
    function CreateQuery: IDBQuery; override;
    function CreateResultSet: IDBResultSet; override;
    function ExecuteSQL(const ASQL: string): IDBResultSet; override;
  end;

  TDriverQueryZeos = class(TDriverQuery)
  private
    FSQLQuery: TZReadOnlyQuery;
  protected
    procedure SetCommandText(ACommandText: string); override;
    function GetCommandText: string; override;
  public
    constructor Create(AConnection: TZConnection);
    destructor Destroy; override;
    procedure ExecuteDirect; override;
    function ExecuteQuery: IDBResultSet; override;
  end;

  TDriverResultSetZeos = class(TDriverResultSet<TZReadOnlyQuery>)
  public
    constructor Create(ADataSet: TZReadOnlyQuery); override;
    destructor Destroy; override;
    function NotEof: Boolean; override;
    function GetFieldValue(AFieldName: string): Variant; overload; override;
    function GetFieldValue(AFieldIndex: Integer): Variant; overload; override;
  end;

implementation

{ TDriverZeos }

constructor TDriverZeos.Create(AConnection: TComponent; ADriverName: TDriverName);
begin
  inherited;
  FConnection := AConnection as TZConnection;
  FDriverName := ADriverName;
end;

destructor TDriverZeos.Destroy;
begin
  FConnection := nil;
  inherited;
end;

procedure TDriverZeos.Disconnect;
begin
  inherited;
  FConnection.Connected := False;
end;

function TDriverZeos.ExecuteSQL(const ASQL: string): IDBResultSet;
var
  oDBQuery: IDBQuery;
begin
  oDBQuery := TDriverQueryZeos.Create(FConnection);
  oDBQuery.CommandText := ASQL;
  Result := oDBQuery.ExecuteQuery;
end;

procedure TDriverZeos.Connect;
begin
  inherited;
  FConnection.Connected := True;
end;

function TDriverZeos.InTransaction: Boolean;
begin
  inherited;
  Result := FConnection.InTransaction;
end;

function TDriverZeos.IsConnected: Boolean;
begin
  inherited;
  Result := FConnection.Connected = True;
end;

function TDriverZeos.CreateQuery: IDBQuery;
begin
  Result := TDriverQueryZeos.Create(FConnection);
end;

function TDriverZeos.CreateResultSet: IDBResultSet;
var
  oDBQuery: IDBQuery;
begin
  oDBQuery := TDriverQueryZeos.Create(FConnection);
  Result   := oDBQuery.ExecuteQuery;
end;

{ TDriverDBExpressQuery }

constructor TDriverQueryZeos.Create(AConnection: TZConnection);
begin
  if AConnection <> nil then
  begin
     FSQLQuery := TZReadOnlyQuery.Create(nil);
     try
       FSQLQuery.Connection := AConnection;
     except
       FSQLQuery.Free;
       raise;
     end;
  end;
end;

destructor TDriverQueryZeos.Destroy;
begin
  FSQLQuery.Free;
  inherited;
end;

function TDriverQueryZeos.ExecuteQuery: IDBResultSet;
var
  oResultSet: TZReadOnlyQuery;
  iFor: Integer;
begin
  oResultSet := TZReadOnlyQuery.Create(nil);
  try
    oResultSet.Connection := FSQLQuery.Connection;
    oResultSet.SQL.Text := FSQLQuery.SQL.Text;

    for iFor := 0 to FSQLQuery.Params.Count - 1 do
    begin
      oResultSet.Params[iFor].DataType := FSQLQuery.Params[iFor].DataType;
      oResultSet.Params[iFor].Value    := FSQLQuery.Params[iFor].Value;
    end;
    oResultSet.Open;
  except
    oResultSet.Free;
    raise;
  end;
  Result := TDriverResultSetZeos.Create(oResultSet);
  if oResultSet.RecordCount = 0 then
     Result.FetchingAll := True;
end;

function TDriverQueryZeos.GetCommandText: string;
begin
  Result := FSQLQuery.SQL.Text;
end;

procedure TDriverQueryZeos.SetCommandText(ACommandText: string);
begin
  inherited;
  FSQLQuery.SQL.Text := ACommandText;
end;

procedure TDriverQueryZeos.ExecuteDirect;
begin
  FSQLQuery.ExecSQL;
end;

{ TDriverResultSetZeos }

constructor TDriverResultSetZeos.Create(ADataSet: TZReadOnlyQuery);
begin
  inherited;
  FDataSet:= ADataSet;
end;

destructor TDriverResultSetZeos.Destroy;
begin
  FDataSet.Free;
  inherited;
end;

function TDriverResultSetZeos.GetFieldValue(AFieldName: string): Variant;
var
  oField: TField;
begin
  oField := FDataSet.FieldByName(AFieldName);
  Result := GetFieldValue(oField.Index);
end;

function TDriverResultSetZeos.GetFieldValue(AFieldIndex: Integer): Variant;
begin
  if FDataSet.Fields[AFieldIndex].IsNull then
     Result := Variants.Null
  else
     Result := FDataSet.Fields[AFieldIndex].Value;
end;

function TDriverResultSetZeos.NotEof: Boolean;
begin
  if not FFirstNext then
     FFirstNext := True
  else
     FDataSet.Next;

  Result := not FDataSet.Eof;
end;

end.
